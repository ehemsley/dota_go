# Dota GO
An objective-based custom game for Dota 2.

Created by Evan Hemsley

Special thanks to BMD for the amazing Barebones framework.

## Hard-linking the git repo to the dota 2 addons folder
from the windows command line

`mklink /J <dota 2 addons game folder> <repo game folder>`

`mklink /J <dota 2 addons content folder> <repo content folder>`
